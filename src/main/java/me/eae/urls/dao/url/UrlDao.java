package me.eae.urls.dao.url;

import me.eae.urls.modle.SinaShortUrl;
import me.eae.urls.modle.Url;

/**
 * @author ceshi
 * @Title: ${file_name}
 * @Package ${package_name}
 * @Description: ${todo}
 * @date 2018/3/3014:23
 */
public interface UrlDao {

    void saveUrl(Url url);

    Url getUrl(String id);

    String getSurl(String md5);

    void saveSurl(String md5,String id);

    /**
     * 获得新浪短链接
     * @param longUrl 长连接
     * @return
     */
    SinaShortUrl getSinaShortUrl(String longUrl);

}
