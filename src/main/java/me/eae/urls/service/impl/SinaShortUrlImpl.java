package me.eae.urls.service.impl;

import me.eae.urls.dao.url.UrlDao;
import me.eae.urls.modle.SinaShortUrl;
import me.eae.urls.service.SinaShortUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 新浪短链接生成
 * @author YI
 * @date 2018-4-11 19:11:24
 */
@Service
public class SinaShortUrlImpl implements SinaShortUrlService {
    @Autowired
    UrlDao urlDao;

    @Override
    public SinaShortUrl getShortUrl(String longUrl) {
        if(!longUrl.startsWith("http://")&&!longUrl.startsWith("https://")){
            longUrl = new StringBuffer().append("http://"+longUrl).toString();
        }

        return urlDao.getSinaShortUrl(longUrl);
    }
}
