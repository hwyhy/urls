package me.eae.urls.controller.url;

import me.eae.urls.modle.Result;
import me.eae.urls.modle.SinaShortUrl;
import me.eae.urls.modle.Url;
import me.eae.urls.service.SinaShortUrlService;
import me.eae.urls.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

/**
 * @author ceshi
 * @Title: ${file_name}
 * @Package ${package_name}
 * @Description: ${todo}
 * @date 2018/3/3015:22
 */
@Controller
@RequestMapping("/")
public class UrlController {
    @Autowired
    UrlService urlService;
    @Autowired
    SinaShortUrlService sinaShortUrlService;

    @GetMapping("/")
    public String index() {
        return "/index";
    }

    @GetMapping("/{id}")
    public String redirect(@PathVariable("id") String id) {
        Url u = urlService.getUrl(id);
        if(u==null){
            return "/404";
        }
        return "redirect:"+u.getUrl();
    }

    @PostMapping("/getShortUrl")
    @ResponseBody
    public Result getShortUrl(Url url){

        String urls = urlService.saveUrl(url);
        return Result.OK(urls);
    }

    @PostMapping("/getSinaShortUrl")
    @ResponseBody
    public Result getSinaShortUrl(String url){

        SinaShortUrl shortUrl = sinaShortUrlService.getShortUrl(url);
        return Result.OK(shortUrl.getUrls().get(0).getUrl_short());
    }

}
