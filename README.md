## urls 短链接生成服务
[Fork自urls项目](https://gitee.com/hao_jiayu/urls)

- 将key使用配置文件管理,优化代码结构
- 新增新浪短链接地址生成

#### 此服务实现的想法来自于知乎对于短链接生成的一些讨论，[地址](https://www.zhihu.com/question/29270034)。
#### 本人是参考 @iammutex这位大神的想法来实现的，但有所不同的是，为保证同一个长地址每次转出来都是一样的短地址，这位的想法是创建一个临时表，储存最近几个小时内生成的短链接，过期后淘汰。
#### 本人的想法是 创建一个 key-value，key为长连接的md5值，value为该长连接对应的短链接。


## 配置部署
config/config.properties
```
# Redis key
key = redis.key.url
# Redis key_md5
keyMd5 = redis.key.url_dm5
# Redis sina key
keySina = redis.key.sina

# 新浪短链接
api = https://api.weibo.com/2/short_url/shorten.json?
source = 1950792609
```
application.properties
```
# REDIS (RedisProperties)
# Redis数据库索引（默认为0）
spring.redis.database=0
# Redis服务器地址
spring.redis.host=localhost
# Redis服务器连接端口
spring.redis.port=6379
# Redis服务器连接密码（默认为空）
spring.redis.password=

# 连接超时时间（毫秒）
spring.redis.timeout=500
# 指定redis生成器初始值，最小为1，最大为1024
me.eae.urls.idGenerator.RedisIdGenerator.startNum=1
```
## 项目截图
![](https://i.imgur.com/ggqkfQ9.jpg)

## 规划
### 增加api调用方式
### 增加统计功能

## 问题建议

- 联系我的邮箱：ilovey_hwy@163.com